#include <iostream>

using namespace std;

constexpr auto val(int i) {
	if (i < 3) { return i + 2; }

	int* ptr = new int(1);
	return i-2;
}

int main() {
	
	constexpr auto v1 = val(2);
	auto v2 = val(3);
	return 0;
}

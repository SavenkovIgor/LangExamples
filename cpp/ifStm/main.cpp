#include <iostream>

using namespace std;

auto f() {
	pair<int, bool> p(0, true);
	return p;
}

int main() {

	if (auto [first, second] = f(); second) {
		cout << "true" << endl;
		cout << typeid(first).name();
		cout << typeid(second).name();
	} else {
		cout << "false" << endl;
		cout << typeid(first).name();
		cout << typeid(second).name();
	}

	cout << endl;

	return 0;
}

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>

using namespace std;

int main() {

	vector<int> v {1, 2, 3, 4, 5};

	cout << accumulate(v.begin(), v.end(), 0) << endl;

	for_each(v.begin(), v.end(), [] (int i) { cout << i << " "; } );
	cout << endl;

	int sum = 0;

	for_each(v.begin(), v.end(), [&sum] (int i) { sum += i; } );

	cout << sum << endl;

	return 0;
}

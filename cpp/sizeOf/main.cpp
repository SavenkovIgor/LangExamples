#include <iostream>

struct Sparce {
	bool b;
	int i;
	bool b1;
	double d;
};

int main()
{
	std::cout << "bool+int+bool+double " << sizeof(Sparce) << std::endl;
	std::cout << typeid(bool).name()   << " " << sizeof(bool) << std::endl;
	std::cout << typeid(int).name()    << " " << sizeof(int) << std::endl;
	std::cout << typeid(double).name() << " " << sizeof(double) << std::endl;
	return 0;
}

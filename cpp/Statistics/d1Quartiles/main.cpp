#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> leftHalf(const vector<int>& input) {
    unsigned int halfSize = static_cast<unsigned int>(input.size()) /2;
    vector<int> ret(halfSize, 0);
    auto middle = input.begin() + halfSize;
    copy(input.begin(), middle, ret.begin());
	return ret;
}

vector<int> rightHalf(const vector<int>& input) {
    unsigned int evenOdd = input.size()%2;
    unsigned int halfSize = static_cast<unsigned int>(input.size()) /2;
    vector<int> ret(halfSize, 0);
    auto middle = input.begin() + halfSize + evenOdd;
    copy(middle, input.end(), ret.begin());
    return ret;
}

float median(const vector<int>& input) {
    unsigned int half = static_cast<unsigned int>(input.size())/2;
    if (input.size()%2 == 0) {
        return static_cast<float>(input[half-1] + input[half]) /2.0f;
    } else {
        return static_cast<float>(input[half]);
    }
}

int main()
{
	int count = 0;
    cin >> count;

	vector<int> v;
    int tmp = 0;

	for (int i = 0; i < count; i++) {
		cin >> tmp;
		v.push_back(tmp);
	}

    sort(v.begin(), v.end());

    cout << median(leftHalf(v)) << endl;
    cout << median(v) << endl;
    cout << median(rightHalf(v)) << endl;
}

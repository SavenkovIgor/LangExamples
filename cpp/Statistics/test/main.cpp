#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

void print(vector<int> v) {
    for_each(v.begin(), v.end(), [] (int i) { cout << i << " "; } );
}

void multiply2(int& i) {
    i*=2;
}

double avgSum(const vector<int>& v) {
    return static_cast<double>( accumulate(v.begin(), v.end(), 0)) / static_cast<double>(v.size());
}

template<typename t>
void printType(t value) {
    cout << typeid(value).name() << endl;
}

int main()
{
    vector<int> v {1, 2, 3};

    print(v);
    for_each(v.begin(), v.end(), multiply2);
    cout << endl;
    print(v);
    cout << endl;
    printType(v);
    cout << endl;

    string s;
    for_each(v.begin(), v.end(), [&s] (int e) { s += to_string(e) + " "; });

    printType(s);
    cout << s << endl;
    cout << avgSum(v);
}

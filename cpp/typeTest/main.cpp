#include <iostream>

using namespace std;

int v(int s) {
	cout << s << endl;
}

int v(double) = delete;

int main() {
	v(7);
	v(7.1);
	return 0;
}

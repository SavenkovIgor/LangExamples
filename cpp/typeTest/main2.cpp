#include <vector>
#include <iostream>
#include <string>

auto out = [] (auto a) { std::cout << a << std::endl; };

template<typename T1, typename T2> void print(const char* name1, const char* name2) { 
	std::cout << std::boolalpha;
	std::cout << name1 << '|' << name2 << '|' << std::is_same<T1, T2>::value << std::endl;
}

void f(std::nullptr_t n) { }

int main()
{
	print<int, int>("int", "int");
	print<int, const int>("int", "const int");
	print<int, int*>("int", "int*");

	print<char, signed char>("char", "signed char");
	print<char, unsigned char>("char", "unsigned char");

	print<int, signed int>("int", "signed int");
	print<int, unsigned int>("int", "unsigned int");
	print<unsigned int, unsigned>("unsigned int", "unsigned");
	print<decltype(nullptr), std::nullptr_t>("nullptr", "nullptr_t");
	
	
	out(typeid(nullptr).name());

	char* a = nullptr;
	f(nullptr);

	out(std::is_function<decltype(out)>::value);
	
	return 0;
}

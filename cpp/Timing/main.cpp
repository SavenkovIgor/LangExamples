#include<iostream>
#include<chrono>
#include<functional>

using namespace std;
using namespace std::chrono;

void measure(function<void()> measuringFunc, unsigned int execCount = 10000) {
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	for (unsigned int i = 0; i < execCount; i++) {
		measuringFunc();
	}
	high_resolution_clock::time_point t2 = high_resolution_clock::now();

	auto duration = duration_cast<microseconds>( t2 - t1 ).count();

	cout << execCount << " cycles executed in " << duration << endl;
}

void f1() {
	double m = 0;
	m++;
}

void f2() {
	int m = 0;
	m++;
}

int main() {
	measure(f1, 1000000);
	measure(f2, 1000000);
	return 0;
}

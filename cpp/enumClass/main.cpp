#include <iostream>

enum class Test 
{
	one,
	two
};

int main()
{
	auto out = [] (const char* c) { std::cout << c << std::endl; };

	Test test;
	switch (test)
	{
		case Test::one:
			out("one");
			break;
		case Test::two:
			out("two");
			break;
		default:
			out("default");
			break;
	}

	return 0;
}


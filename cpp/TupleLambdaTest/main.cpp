#include<vector>
#include<iostream>
#include<tuple>
#include<functional>
#include<algorithm>

using namespace std;

int sum(int a, int b) {
	return a+b;
}

int mul(int a, int b) {
	return a*b;
}

int main() {

	vector< tuple< int, int, std::function<int (int, int)>>> v;

	v.push_back( make_tuple(2, 3, sum) );
	v.push_back( make_tuple(2, 3, mul) );
	v.push_back( make_tuple(2, 3, [] (auto a, auto b) { return a/b; } ) );

	for_each(v.begin(), v.end(), [] (auto a) { cout << get<2>(a)(get<0>(a), get<1>(a)) << endl; } );

	cout << "--------------" << endl;
	
	for(auto [n1, n2, func] : v) {
		cout << func(n1,n2) << endl;
	}

	return 0;
}

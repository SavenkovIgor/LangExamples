#include <iostream>
#include <memory>

using namespace std;

class SomeClass {
	public:
	SomeClass() {
		cout << "Class created!" << endl;
	}
	~SomeClass() {
		cout << "Class destructed" << endl;
	}
};

void checkPtr(const shared_ptr<SomeClass>& p, string prefix = "") {
	cout << prefix << " UseCount " << p.use_count();
	if (p) {
		cout << " has value " << endl;
	} else {
		cout << " no value" << endl;
	}
}

int main() {

	shared_ptr<SomeClass> somePtr = make_shared<SomeClass>();

	checkPtr(somePtr);

	cout << "Assing nullptr" << endl;
	somePtr = nullptr;

	checkPtr(somePtr, "after nullPtr");
	
	//for (int i = 0; i < 100000000; i++) {
	//	somePtr = make_shared<int>(6);
	//	int* im = new int;
	//}

	//cout << "ended" << endl;

	//int x = 0;
	//cin >> x;

	return 0;
}



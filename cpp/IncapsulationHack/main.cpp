#include <iostream>

using namespace std;

class A {
public:
	virtual void f() { cout << "A" << endl; }
};

class B : public A {
private:
	void f() { cout << "B" << endl; }
};

void print(A &a) { a.f(); }

int main() {
	B b;
	print(b);
	return 0;
}

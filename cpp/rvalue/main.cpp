#include <iostream>

using namespace std;

void f(int&  i) { cout << "ref " << i << endl; }
void f(int&& i) { cout << "rval " << i << endl; } 


int main() 
{
	int i = 2;
	f(1);
	f(i);
	f((1));
	f((i));
	f(1);
	f(std::move(i));
	f(std::move(i));
	return 0;
}

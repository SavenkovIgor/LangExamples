#include<iostream>
#include<thread>

using namespace std;

const int count = 1000;

void out(string c) {
	for (int i = 0; i < count; i++) {
		cout << c;
	}
}

int main() {
	thread t1(out,"-");
	thread t2(out,"+");
	t1.join();
	t2.join();

	return 0;
}

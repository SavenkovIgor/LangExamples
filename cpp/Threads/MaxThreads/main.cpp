#include <iostream>
#include <thread>

int main() {
	auto max = std::thread::hardware_concurrency();
	std::cout << "Max threads: " << max << std::endl;

	return 0;
}

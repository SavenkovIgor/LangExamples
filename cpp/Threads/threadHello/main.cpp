#include <iostream>
#include <thread>

using namespace std;

void print() {
	cout << "hello thread!" << endl;
}

int main() {
	thread t(print);
	t.join();
	return 0;
}

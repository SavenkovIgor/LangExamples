#include<thread>
#include<iostream>

using namespace std;

const int count = 5;

struct c {

	char sym;

	c(char c) : sym(c) { }

	void operator ()() {
		h1();
	}

	void h1() {
		for (int i = 0; i < count; i++ ) {
			cout << sym;
		}
	}
};

int main() {

	for (int i = 46; i < 100; i++) {
		c tmp(static_cast<char>(i));
		thread t(tmp);
		//t.join();
		t.detach();
	}

	cout << endl << "===== Return 0 =====" << endl;
	return 0;
}

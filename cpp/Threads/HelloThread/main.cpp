#include<thread>
#include<iostream>

void h() {
	std::cout << "Hello Thread!" << std::endl;
}

int main() {
	std::thread t(h);
	t.join();

	return 0;
}
